# ci-templates

Templates for various tasks in GitLab pipelines.

[[_TOC_]]

## cypress.yml

Use [Cypress](https://www.cypress.io/) for integration tests.

### .cypress

Uses the upstream Cypress image and uploads screenshots/images if the tests fail. Override the `script` to start the tests of your project.

## fluxctl.yml

A job to provide the `fluxctl` binary of the [flux project](https://fluxcd.io/).

### .fluxctl

Extend the job and override the `script` section with your commands.

Optional: Set `FLUXCTL_VERSION` to use a specific version.

## helmfile.yml

Jobs to run helmfile. To use a `helmfile.yaml` from another directory, run `cd ...` in `before_script:`.

### .helmfile-apply

Run `helmfile apply` in the current directory.

### .helmfile-diff

Run `helmfile diff` in the current directory.

## kaniko.yml

Base jobs to build Docker images with [kaniko](https://github.com/GoogleContainerTools/kaniko).

### .kaniko-gitlab

Login kaniko to the GitLab registry of the current project.

## kubectl.yml

A job to provide an image with `kubectl` installed. Make sure to add `$KUBECONFIG` as a file variable to the CI.

### .kubectl

Extend the job and override the `script` section with your commands.

Optional: Set `KUBECTL_VERSION` to use a specific version, defaults to the current stable release.

## prettier.yml

Use [Prettier](https://prettier.io) to lint all kinds of files. Usually it isn't necessary to configure Prettier, but in case it is, check the [docs](https://prettier.io/docs/en/configuration.html).

### .prettier

Lint all supported file types that match `PATTERN`. `PATTERN` can be a file, a directory or a [glob pattern](https://github.com/mrmlnc/fast-glob#pattern-syntax). The default is `.`.

## Renovate

Run [Renovate](https://github.com/renovatebot/renovate/) for the current repository to update dependencies. Make sure to define access tokens for GitLab (`RENOVATE_TOKEN`) and GitHub (`GITHUB_COM_TOKEN`). The GitLab token needs api permissions to manage merge requests. The GitHub token is used to fetch release notes and does not require any permissions (Renovate does only access public information on GitHub, but without a token it would hit rate limits).

### .renovate

The commit author can be overriden via `RENOVATE_GIT_AUTHOR: Some Name <somename@example.com>`. For testing it can be useful to set `RENOVATE_DRY_RUN: "true"`. The job only runs for scheduled pipelines by default, this can be changed by overriding the `only` field of the job.

## telegram.yml

Send messages to Telegram chats from a GitLab pipeline. The message is simply passed to curl without any care of escaping. So please put only simple strings in there. It is required to create a Telegram bot first by talking to the [bot father](https://core.telegram.org/bots#6-botfather).

### .telegram

Extend the job and set the following variables:

- `TELEGRAM_TOKEN`
- `TELEGRAM_CHAT_ID`
- `TELEGRAM_MESSAGE`

## yamllint.yml

### .yaml-lint

Lint all files matching `*.yml` or `*.yaml` using [yamllint](https://yamllint.readthedocs.io/). Set `DIRECTORY` to only check a subdirectory, defaults to `.`. Put a `.yamllint` file into the directory to configure yamllint.
